# -*- coding: utf-8 -*-
"""Przykład dla obiektu typu set"""


def show_set():
	"""
	Prezentacja kolekcji set
	:return:none
	"""
	set_value1 = {'t', 'e', 's', 't', 'o', 'w', 'a', 'n', 'i', 'e'}
	set_value2 = set('qwerty')
	print(f'set_value1: {set_value1}')
	print(f'set_value2: {set_value2}')
	print(f'Wynik funkcji [łączenie zbiorów]union: {set_value1.union(set_value2)}')
	print(f'Wynik funkcji [różne elementy]difference: {set_value1.difference(set(set_value2))}')
	print(f'Wynik funkcji [sortowanie]sorted: {sorted(list(set_value1))}')
	print(f'Wynik funkcji [część wspólna]intersection: {set_value1.intersection(set_value2)}')


def main():
	"""
	Funkcja główna
	"""
	show_set()


if __name__ == '__main__':
	main()
