# -*- coding: utf-8 -*-
"""Przykład dla obiektu typu str"""


def show_str():
	"""
	Prezentacja kolekcji string
	:return:none
	"""
	value_str = "testowanie"
	print(
		r"""
		0       1       2       3       4       5       6       7       8       9       10
		|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|
		|   t   |   e   |   s   |   t   |   o   |   w   |   a   |   n   |   i   |   e   |
		|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|
		-10     -9      -8      -7      -6      -5      -4      -3      -2      -1

			0		1		2		3		4		5		6		7		8       9
		|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|
		|   t   |   e   |   s   |   t   |   o   |   w   |   a   |   n   |   i   |   e   |
		|-------|-------|-------|-------|-------|-------|-------|-------|-------|-------|
			-10    -9      -8      -7      -6      -5      -4      -3      -2      -1
		"""
	)

	print(f'WYNIK 1: {value_str[3::-1]}')
	print(f'WYNIK 2: {value_str[-4:10]}')
	print(f'WYNIK 3: {value_str[3] + value_str[4] + value_str[5]}')
	print(value_str[-10])
	print(value_str[-9])


def main():
	"""
	Funkcja główna
	"""
	show_str()


if __name__ == '__main__':
	main()
