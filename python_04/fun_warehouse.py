# -*- coding: utf-8 -*-
"""Przykład dla obiektu typu słownik"""

# słownik przedmiotów
dict_warehouse = {"woda": 6}


def show_warehouse():
	"""
	Wyświetlenie zawartości słownika magazynu
	:return: none
	"""
	print('NAZWA    | LICZBA')
	for key, value in dict_warehouse.items():
		print(f'{key} | {value} ')
	print()


def add_value(dict_key: str, dict_value: int):
	"""
	Dodanie wartości do słownika
	:param dict_key: klucz słownika, nazwa przedmiotu do dodania
	:param dict_value: wartość słownika, wartość przedmiotu
	:return: none
	"""
	dict_warehouse.setdefault(dict_key, dict_value)
	print(f'Magazyn uzupełniony :{dict_key} liczba: {dict_value}')


def set_dict_value(dict_key: str, dict_value: int):
	"""
	Zmiana wartości klucz dla słownika
	:param dict_key: klucz słownika
	:param dict_value: wartość słownika
	:return: none
	"""
	is_key_dict = dict_warehouse.get(dict_key, '#brak')
	if is_key_dict == '#brak':
		print(f'Brak {dict_key}')
	else:
		dict_warehouse[dict_key] = dict_value
		print(f'Zmiana została wykonana dla {dict_key}: {dict_warehouse[dict_key]}')


def del_dict_key(dict_key: str):
	"""
	Usuniecie klucza z słownika
	:param dict_key: klucz słownika
	:return:none
	"""
	is_key_dict = dict_warehouse.get(dict_key, '#brak')
	if is_key_dict == '#brak':
		print(f'Brak {dict_key}')
	else:
		del dict_warehouse[dict_key]
		print(f'Usunięto : {dict_key}')


def show_menu():
	"""
	Prezentacja menu aplikacji
	:return: wyświetlenie menu
	"""
	print('+------------------------------+')
	print('|           magazyn            |')
	print('+------------------------------+')
	print('|1. Wyświetl zawartość         |')
	print('|2. Dodaj                      |')
	print('|3. Usuń                       |')
	print('|4. Zmień                      |')
	print('|Q. Koniec                     |')
	print('+------------------------------+')


def main():
	"""
	Funkcja główna
	"""
	user_choice = ''
	while user_choice.upper() != 'Q':
		show_menu()
		user_choice = input('Wybierz opcja z menu\n')
		if user_choice == '1':
			show_warehouse()
		elif user_choice == '2':
			dict_key = input("Podaj nazwę do dodania:\n")
			dict_value = int(input("Podaj liczbę:\n"))
			add_value(dict_key=dict_key, dict_value=dict_value)
		elif user_choice == '3':
			dict_key = input("Podaj nazwę do usunięcia:\n")
			del_dict_key(dict_key=dict_key)
		elif user_choice == '4':
			dict_key = input("Podaj nazwę do zmiany:\n")
			dict_value = int(input("Podaj nową liczbę:\n"))
			set_dict_value(dict_key=dict_key, dict_value=dict_value)
		elif user_choice.upper() == 'Q':
			break
		else:
			print('Wybierz poprawną opcję')


if __name__ == '__main__':
	main()
