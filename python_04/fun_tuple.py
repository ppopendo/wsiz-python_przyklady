# -*- coding: utf-8 -*-
"""Przykład dla obiektu typu tuple krotka"""


def show_tuple():
	"""
	Prezentacja kolekcji tuple
	:return:none
	"""
	# tworzenie krotki tuple
	tuple_empty = ()
	print(tuple_empty)

	# krotka liczbowa
	tuple_num = (5, 2, 9, 87)
	print(tuple_num)

	# krotka tekstowa
	tuple_str = 'to', 'jest', '0123', 'analiza', 'programowanie'

	# krotka multi
	tuple_multi = ('0123', [0, 1, 2], (1, 2, 3))
	print(tuple_multi)
	print('*' * 100)
	print(tuple_multi[0][2])
	print('*' * 100)

	# krotka bez nawiasów
	tuple_without_parentheses = 5, 7.1, 'to jest 0123'
	print(tuple_without_parentheses)

	# rozpakowanie krotki
	x, y, z = tuple_without_parentheses
	print(x)
	print(y)
	print(z)

	# dostęp do krotki
	print(tuple_multi[2][0])
	print(tuple_multi[2])
	print(tuple_num[3])
	print(tuple_str[0])
	print(tuple_str[0:2])

	# usuwanie
	del tuple_num


def main():
	"""
	Funkcja główna
	"""
	show_tuple()


if __name__ == '__main__':
	main()
