# -*- coding: utf-8 -*-
"""Przykład dla obiektu typu lista"""


# lista elementów plecaka
list_backpack = ['latarka', 'mapa']


def add_item_to_backpack(item_bag: str):
	"""
	Dodanie przedmiotu do plecaka
	:return: zmiana zawartości plecaka
	"""
	list_backpack.append(item_bag)
	list_backpack.insert(0, item_bag)
	print(f'Przedmiot został dodany id: {list_backpack.index(item_bag)}')


def print_list_backpack():
	"""
	Wyświetlenie zawartości plecaka opcja 1
	:return: Wyświetlenie zawartości plecaka
	"""
	print()
	print('ID | PRZEDMIOT')
	for cnt, item in enumerate(iterable=list_backpack, start=0):
		print(str(cnt) + '  | ' + str(item))


def show_menu():
	"""
	Prezentacja menu aplikacji
	:return: Wyświetlenie menu
	"""
	print('+-------------------------------+')
	print('|           Backpack            |')
	print('+-------------------------------+')
	print('|1. Wyświetl zawartości         |')
	print('|2. Dodaj przedmiot             |')
	print('|3. Usuń przedmiot              |')
	print('|4. Zmiana nazwy                |')
	print('|Q. Wyjście                     |')
	print('+------------------------------+')


def main():
	"""
	Funkcja główna
	"""
	user_opt = ''
	while user_opt.upper() != 'Q':
		show_menu()
		user_opt = input('Wybierz opcja z menu\n')
		if user_opt == '1':
			print_list_backpack()
		elif user_opt == '2':
			item_backpack = input('Podaj nazwa przedmiotu do dodania\n')
			add_item_to_backpack(item_bag=item_backpack)
		elif user_opt == '3':
			print_list_backpack()
			item_backpack = int(input('Podaj id przedmiotu do usunięcia\n'))
			list_backpack.remove(list_backpack[item_backpack])
			# lub
			# print('Przedmiot \"{0}\" został usunięty'.format(list_backpack.pop(item_backpack)))
			print()
		elif user_opt == '4':
			print_list_backpack()
			item_backpack = int(input('Podaj id przedmiotu do zmiany\n'))
			name_item_backpack = input('Podaj nową nazwę\n')
			list_backpack[item_backpack] = name_item_backpack
			print('Zmiana wykonana')
		elif user_opt.upper() == 'Q':
			break
		else:
			print('Wybrano błędną opcje')


if __name__ == '__main__':
	main()
