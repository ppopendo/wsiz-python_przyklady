# -*- coding: utf-8 -*-
"""Przykład dla funkcji print"""


value_str1 = 'Czy łorst kejs scenario uwzględnia tego kejsa?'
value_str2 = 'Podstawy programowania Python'
value_str3 = 'Nie mam klu o czym był ten kol'
print(f'value_str1 {value_str1}')
print(f'value_str2 {value_str2}')
print(f'value_str3 {value_str3}')
print(f'isupper() value_str1: {value_str1.isupper()}')
print(f'upper() zmiana z: {value_str1} zmiana na: {value_str1.upper()}')
print(f'islower() value_str2: {value_str2.islower()}')
print(f'lower() zmiana z: {value_str2} zmiana na: {value_str2.lower()}')
print(f'isalpha() value_str2: {value_str2.isalpha()}')
print(f'isalnum() value_str2: {value_str2.isalnum()}')
print(f'isdecimal() value_str2: {value_str2.isdecimal()}')
print(f'isspace() value_str2: {value_str2.isspace()}')
print(f'istitle() value_str2: {value_str2.istitle()}')
print(f'capitalize() zmiana z: {value_str2} zmiana na: {value_str2.capitalize()}')
print(f"split() value_str2: {value_str2.split(' ')}")
