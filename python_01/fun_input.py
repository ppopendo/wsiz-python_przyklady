# -*- coding: utf-8 -*-
"""Przykład dla funkcji input"""


value_x = int(input('Podaj wartość X: '))
value_y = int(input('Podaj wartość Y: '))
print('Operatory matematyczne')
print(f'Dodawanie: {value_x} + {value_y} = {value_x + value_y} ')
print(f'Odejmowanie: {value_x} - {value_y} = {value_x - value_y} ')
print(f'Mnożenie: {value_x} * {value_y} = {value_x * value_y} ')
print(f'Dzielenie: {value_x} / {value_y} = {value_x / value_y} ')
print(f'Modulo: {value_x} % {value_y} = {value_x % value_y} ')
print(f'Dzielenie całkowite: {value_x} // {value_y} = {value_x // value_y} ')
print(f'Potęga: {value_x} ** {value_y} = {value_x ** value_y} ')
