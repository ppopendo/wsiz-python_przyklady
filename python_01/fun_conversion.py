# -*- coding: utf-8 -*-
"""Przykład dla funkcji konwersja danych, operatory przypisania"""


value_x = int(input('Podaj wartość X: '))
value_y = int(input('Podaj wartość Y: '))
value_x_y = value_y + value_x
str_line = 100 * '-'
print(str_line)
print('\t\t\tOperatory przypisania')
print(str_line)
print(f'Przypisania:  value_x_y {value_x_y} = value_y {value_y} +  value_x {value_x} ')
print(f'Dodawanie: {value_x} += {value_y} ')
value_x += value_y
print(value_x)
print(f'Odejmowanie: {value_x} -= {value_y} ')
value_x -= value_y
print(value_x)
print(f'Mnożenie: {value_x} *= {value_y} ')
value_x *= value_y
print(value_x)
print(f'Dzielenie: {value_x} /= {value_y} ')
value_x /= value_y
print(value_x)
print(f'Modulo: {value_x} %= {value_y} ')
value_x %= value_y
print(value_x)
print(f'Potęga: {value_x} **= {value_y} ')
value_x **= value_y
print(value_x)
print(f'Dzielenie całkowite: {value_x} //= {value_y} ')
value_x //= value_y
print(value_x)
print(str_line)
print('\t\t\tKonwersja danych')
print(str_line)
print(str_line)
print('Implicit')
print(str_line)
value_int = 5
value_flo = 5.5
sum_int_flo = value_int + value_flo
print(f'value_int {value_int}')
print(f'value_flo {value_flo}')
print(f'Typ danych value_int {type(value_int)}')
print(f'Typ danych value_flo {type(value_flo)}')
print(f'Typ danych sum_int_flo {type(sum_int_flo)}')
print(str_line)
print('Explicit')
print(str_line)
value_int = 5
value_str = '123'
print(f'value_int {value_int}')
print(f'value_str {value_str}')
print('Typy danych przed konwersja')
print(f'Typ danych value_int {type(value_int)}')
print(f'Typ danych value_str {type(value_str)}')
value_str = int(value_str)
sum_int_str = value_int + value_str
print('Typy danych po konwersji')
print(f'Typ danych value_int {type(value_int)}')
print(f'Typ danych value_str {type(value_str)}')
print(f'Typ danych sum_int_str {type(sum_int_str)}')
