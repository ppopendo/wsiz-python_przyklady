# -*- coding: utf-8 -*-
"""Przykład dla funkcji Loop For na podstawie przykładu beersong z książki
Python. Rusz głową! Wydanie II Książka, kurs - Paul Barry"""

word = "butelki"

for beer_num in range(4, 0, -1):
	print(beer_num, word, "piwa w lodówce.")
	print("Jedną weź.")
	print("Podaj w koło.")
	if beer_num == 1:
		print("Nie ma już butelek piwa w lodówce.")
	else:
		new_num = beer_num - 1
		if new_num == 1:
			word = "butelka"
			print(new_num, word, "piwa w lodówce.")
	print()
