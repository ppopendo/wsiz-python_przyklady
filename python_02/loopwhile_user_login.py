# -*- coding: utf-8 -*-
"""Przykład dla funkcji Loop While - sprawdzenie danych logowania"""


while True:
	user_login = input("Podaj login: ")
	if user_login != "admin":
		continue
	user_password = input("Podaj hasło: ")
	if user_password == "admin123":
		break
print("Hello {0}".format(user_login))
