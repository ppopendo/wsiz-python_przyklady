# -*- coding: utf-8 -*-
"""Przykłady użycia instrukcji if - sprawdź wynik"""
from random import randint


def check_user_data(value_user):
	"""
	Sprawdzenie danych użytkownika
	:param value_user: wartość użytkownika
	:return: znacznik liczbowy
	"""
	if value_user > 100:
		return 0
	if isinstance(value_user, int):
		return 1
	if isinstance(value_user, str):
		return 2
	return 3


def cmp_result(value_user):
	"""
	Sprawdzenie wyniku - porównanie wartości użytkownika z wartością wylosowaną
	:param value_user: wartość użytkownika
	:return: wyświetlenie wyniku
	"""
	random_value = randint(1, 100)
	if value_user == random_value:
		print(r"""
        .----------------.  .----------------.  .-----------------.
        | .--------------. || .--------------. || .--------------. |
        | | _____  _____ | || |     _____    | || | ____  _____  | |
        | ||_   _||_   _|| || |    |_   _|   | || ||_   \|_   _| | |
        | |  | | /\ | |  | || |      | |     | || |  |   \ | |   | |
        | |  | |/  \| |  | || |      | |     | || |  | |\ \| |   | |
        | |  |   /\   |  | || |     _| |_    | || | _| |_\   |_  | |
        | |  |__/  \__|  | || |    |_____|   | || ||_____|\____| | |
        | |              | || |              | || |              | |
        | '--------------' || '--------------' || '--------------' |
         '----------------'  '----------------'  '----------------'
        """)
	else:
		print(r"""
         .----------------.  .----------------.  .----------------.  .----------------.
        | .--------------. || .--------------. || .--------------. || .--------------. |
        | |   _____      | || |     ____     | || |    _______   | || |  _________   | |
        | |  |_   _|     | || |   .'    `.   | || |   /  ___  |  | || | |_   ___  |  | |
        | |    | |       | || |  /  .--.  \  | || |  |  (__ \_|  | || |   | |_  \_|  | |
        | |    | |   _   | || |  | |    | |  | || |   '.___`-.   | || |   |  _|  _   | |
        | |   _| |__/ |  | || |  \  `--'  /  | || |  |`\____) |  | || |  _| |___/ |  | |
        | |  |________|  | || |   `.____.'   | || |  |_______.'  | || | |_________|  | |
        | |              | || |              | || |              | || |              | |
        | '--------------' || '--------------' || '--------------' || '--------------' |
         '----------------'  '----------------'  '----------------'  '----------------'
        """)
	print(f'Wartość użytkownika: {str(value_user)}\nWartość losowa: {str(random_value)}')


def main():
	"""
	Funkcja główna
	:return: none
	"""
	value_user = int(input('Podaj wartość liczbową od 1 do 100 \n'))
	if check_user_data(value_user) == 0:
		print('Wprowadzono błędna liczbę')
	elif check_user_data(value_user) == 1:
		cmp_result(value_user)
	else:
		print('Wprowadzano wartość nie jest liczba')


if __name__ == "__main__":
	main()
