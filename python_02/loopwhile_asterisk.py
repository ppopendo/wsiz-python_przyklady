# -*- coding: utf-8 -*-
"""Przykład dla funkcji Loop While - drukowanie gwiazdek """


count_loop = 100
str_loop = '*'
user_no = int(input("Podaj liczbę: "))
while user_no < count_loop:
	print(str_loop)
	str_loop += '*'
	user_no += 1
