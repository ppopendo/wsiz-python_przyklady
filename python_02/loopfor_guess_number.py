# -*- coding: utf-8 -*-
"""Przykład dla funkcji Loop While - odgadnij liczbę"""
import random


random_value = random.randint(0, 9)
count_sample = 0
print("\t\tGRA: Odgadnij liczbę")
for q in range(10):
	user_value = int(input('Podaj cyfrę od 0 do 9 \n'))
	count_sample += 1
	if random_value == user_value:
		print(f'<<< Koniec gry >>>\nLiczba prób: {count_sample}')
		break
	if random_value != user_value:
		continue
