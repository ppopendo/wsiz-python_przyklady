# -*- coding: utf-8 -*-
"""Przykład dla funkcji Loop While - drukowanie wartości tekstowej iteracyjnie"""


user_str = input("Podaj wartość tekstową:\n")
len_user_str = len(user_str)
count_str = 1
while len_user_str > 0:
	print(user_str[0:count_str])
	len_user_str -= 1
	count_str += 1
