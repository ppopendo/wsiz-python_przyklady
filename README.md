# wsiz-python_przyklady aktualizacja 2020-12-12

## Opis projektu 
* [Informacje](#informacje)
* [Technologie](#technologie)
* [Konfiguracja](#konfiguracja)

## Informacje
Projekt zwiera przykładowe skrypty do nauki podstaw języka programowania Python-a
	
## Technologie
* Python
	
	
## Konfiguracja
1. Pobranie Postman: [Python](https://www.python.org/downloads/ "Python")
2. Pobranie PyCharm: [PyCharm](https://www.jetbrains.com/pycharm/download/ "Aplikacja Pycharm")



## Tabela z opisem skryptów 

### python_01

SKRYPT | OPIS 
------------- | -------------
hello_world.py | Hello World
fun_print.py | Przykład dla funkcji print
fun_input.py | Przykład dla funkcji input
fun_conversion.py | Przykład dla funkcji konwersja danych, operatory przypisania

### python_02

SKRYPT | OPIS 
------------- | -------------
condition_check_result.py | Przykłady użycia instrukcji if - sprawdź wynik
loopfor_beersong.py | Przykład dla funkcji Loop For na podstawie przykładu beersong z książki
loopfor_guess_number.py | Przykład dla funkcji Loop While - odgadnij liczbę
loopwhile_asterisk.py | Przykład dla funkcji Loop While - drukowanie gwiazdek
loopwhile_guess_pwd.py | Przykład dla funkcji Loop While - zgadnij hasło
loopwhile_print_txt.py | Przykład dla funkcji Loop While - drukowanie wartości tekstowej iteracyjnie
loopwhile_user_login.py | Przykład dla funkcji Loop While - sprawdzenie danych logowania

### python_03

SKRYPT | OPIS 
------------- | -------------
fun_args_kwargs.py | Przykład dla funkcji def -  args,kwargs
fun_calc.py | Przykład dla funkcji def/return - wykonywanie działań arytmetycznych
fun_default_value.py | Przykład dla funkcji def - domyślne wartość, typy argumentów
fun_except.py | Przykład dla funkcji def domyślne wartość, typy argumentów
fun_hello_world.py | Przykład dla funkcji def - hello world
fun_import_hello_world.py | Przykład dla funkcji def main import
fun_variables.py | Przykład dla funkcji def - zmienne lokalne, globalne

### python_04

SKRYPT | OPIS 
------------- | -------------
fun_backpack.py | Przykład dla obiektu typu lista  plecak
fun_set.py | Przykład dla obiektu typu set
fun_str.py | Przykład dla obiektu typu str
fun_tuple.py | Przykład dla obiektu typu tuple krotka
fun_warehouse.py | Przykład dla obiektu typu słownik

### python_05

SKRYPT | OPIS 
------------- | -------------
cls_product.py | Przykład dla definiowania klasy