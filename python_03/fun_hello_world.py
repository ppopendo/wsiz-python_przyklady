# -*- coding: utf-8 -*-
"""Przykład dla funkcji def - hello world"""


def hello_world(user_name):
	"""Wyświetlenie komunikatu tekstowego
	:param user_name: nazwa użytkownika
	:return: komunikat tekstowy
	"""
	print('Hello ', user_name)


def main():
	"""
	Funkcja główna
	"""
	print('Podaj nazwę użytkownika:')
	user_name = input()
	# Uruchomienie funkcji hello_world
	hello_world(user_name=user_name)


if __name__ == '__main__':
	main()
