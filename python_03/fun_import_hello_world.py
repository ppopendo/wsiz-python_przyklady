# -*- coding: utf-8 -*-
"""Przykład dla funkcji def main import"""

from python_03 import fun_hello_world as fun1


def main():
	"""
	Funkcja główna
	"""
	print('Podaj nazwę użytkownika:')
	user_name = input()
	# Uruchomienie funkcji HelloWorld z importu
	fun1.hello_world(user_name)


if __name__ == '__main__':
	main()
