# -*- coding: utf-8 -*-
"""Przykład dla funkcji def - wykonywanie działań arytmetycznych"""
import sys


def calculate(value_a, value_b, value_c):
	"""
	Wykonywanie działań arytmetycznych dodawanie/odejmowanie
	:param value_a wartość liczbowa do obliczenia działania arytmetycznego
	:param value_b wartość liczbowa do obliczenia działania arytmetycznego
	:param value_c typ działania arytmetycznego
	:return wynik działania
	"""
	if value_c == '+':
		return value_a + value_b
	return value_a - value_b


def calculate2(value_a, value_b, value_c):
	"""
	Wykonywanie działań arytmetycznych dodawanie/odejmowanie
	:param value_a wartość liczbowa do obliczenia działania arytmetycznego
	:param value_b wartość liczbowa do obliczenia działania arytmetycznego
	:param value_c typ działania arytmetycznego
	:return wynik działania
	"""
	if value_c == '+':
		return value_a + value_b, '[SUMA]'
	return value_a - value_b, '[ODEJMOWANIE]'


def main():
	"""
	Funkcja główna
	"""
	print('Podaj wartość A : ')
	value_a = int(input())
	print('Podaj wartość B: ')
	value_b = int(input())
	print('Podaj znak: [ + lub - ]')
	value_c = input()
	if value_c not in ('+', '-'):
		print("Wprowadzono błędny znak działania")
		sys.exit()
	print(f'calculate  : {value_a} {value_c} {value_b} = {calculate(value_a, value_b, value_c)}')
	print(f'calculate2 : {value_a} {value_c} {value_b} = {calculate2(value_a, value_b, value_c)}')
	print('Kolekcja danych wyjściowych calculate2:')
	for item in calculate2(value_a, value_b, value_c):
		print(item)


if __name__ == '__main__':
	main()
