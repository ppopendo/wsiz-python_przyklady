# -*- coding: utf-8 -*-
"""Przykład dla funkcji def -  args,kwargs"""


def add_value(*value_list):
	"""
	Funkcja do wykonywania działań arytmetycznych - dodawanie
	:param value_list lista wartości do dodania
	:return wynik działania
	"""
	result = 0
	for item in value_list:
		result += item
	print(f'Wynik: {result}')


def print_customer_data(**data_dict):
	"""
	Prezentacja danych klienta
	:param data_dict Słownik danych
	:return prezentacja danych klienta ze słownika data_dict
	"""
	for key, value in data_dict.items():
		print(f'{key} {value}')


def unpack_fun(value_x, value_y):
	"""
	Prezentacja danych rozpakowania danych
	:param value_x:wartość liczbowa do działania arytmetycznego
	:param value_y:wartość liczbowa do działania arytmetycznego
	:return:prezentacja wyniku
	"""
	value_z = value_x + value_y
	print(f'value_x {value_x} + value_y {value_y} = value_z {value_z}')


def main():
	"""
	Funkcja główna
	"""
	print("+--------------------------+")
	print('| Uruchomienie add_value    |')
	print("+--------------------------+")
	add_value(1, 2, 3, 4, 5)
	add_value(1, 2, 3, 4, 5, 6, 7, 8)
	print("+--------------------------+")
	print('| Uruchomienie CustomerData|')
	print("+--------------------------+")
	print_customer_data(
		CustomerId=1, FirstName="Włościwoj", LastName="Poniedziałek", MobilePhone=123456789
	)
	print_customer_data(
		CustomerId=2, FirstName="Uniegost", LastName="Wtorek", Email="testowanie@test.com"
	)
	print_customer_data(Test=1)
	print("+--------------------------+")
	print('| Uruchomienie unpack_fun  |')
	print("+--------------------------+")
	tuple_value = 10, 10
	dict_value = {"value_x": 10, "value_y": 10}
	unpack_fun(*tuple_value)
	unpack_fun(**dict_value)


if __name__ == '__main__':
	main()
