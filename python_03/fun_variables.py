# -*- coding: utf-8 -*-
"""Przykład dla funkcji def - zmienne lokalne, globalne"""


value_global = "TEST_ABC"


def set_global_value():
	"""
	Funkcja testowa do modyfikacji zmiennej globalnej
	:return: prezentacja wartości globalnej
	"""
	global value_global
	value_global = "Hello World"


def set_local_value():
	"""
	Funkcja testowa do prezentacji zmiennej lokalnej
	:return: prezentacja wartości lokalnej
	"""
	value_str = "test"
	value_global = "test2"
	print(f'[Fun set_local_value()] value_str: {value_str} value_global: {value_global}')


def main():
	"""
	Funkcja główna
	"""
	print("Zmienna globalna przed zmianą: ", value_global)
	set_global_value()
	print("Zmienna globalna po zmianie: ", value_global)
	set_local_value()


if __name__ == '__main__':
	main()
