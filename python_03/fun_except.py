# -*- coding: utf-8 -*-
"""Przykład dla funkcji def domyślne wartość, typy argumentów"""


def print_info(value_a, value_b, value_c):
	"""
	Wykonywanie działań arytmetycznych - dodawanie
	:param value_a: wartość liczbowa
	:param value_b: wartość liczbowa
	:param value_c: znak działania arytmetycznego
	:return: suma wartości
	"""
	try:
		if value_c == "+":
			print(f'Wynik {value_a} + {value_b} = {value_a + value_b}')
		elif value_c == "-":
			print(f'Wynik {value_a} - {value_b} = {value_a - value_b}')
		elif value_c == "*":
			print(f'Wynik {value_a} * {value_b} = {value_a * value_b}')
		elif value_c == "/":
			print(f'Wynik {value_a} / {value_b} = {value_a / value_b}')
	except ZeroDivisionError:
		print("Błędna wartość")
	except TypeError:
		print("Błędny typ danych")
	finally:
		print("bye bye")


def main():
	"""
	Funkcja główna
	"""
	value_a = int(input("Podaj wartość liczbową\n"))
	value_b = int(input("Podaj wartość liczbową\n"))
	value_c = input("Wybierz znak działania arytmetycznego + - * / \n")
	print_info(value_a, value_b, value_c)


if __name__ == '__main__':
	main()
