# -*- coding: utf-8 -*-
"""Przykład dla funkcji def - domyślne wartość, typy argumentów"""


def add_value(value_a=1, value_b=1):
	"""
	Wykonywanie działań arytmetycznych - dodawanie
	:param value_a: wartość liczbowa
	:param value_b: wartość liczbowa
	:return: suma wartości
	"""
	sum_value = value_a + value_b
	return sum_value


def add_value2(value_a: int = 1, value_b: int = 1) -> int:
	"""
	Wykonywanie działań arytmetycznych - dodawanie
	:param value_a: wartość liczbowa
	:param value_b: wartość liczbowa
	:return: suma wartości
	"""
	sum_value = value_a + value_b
	return sum_value


def main():
	"""
	Funkcja główna
	"""
	print(f'Wynik funkcji add_value wartości domyślne: {add_value()}')
	print(f'Wynik funkcji add_value z określeniem typów parametrów: {add_value2()}')
	# przykład uruchomień funkcji
	print(add_value2(12, 12))
	print(add_value2(value_b=30, value_a=20))
	print()
	help(add_value)
	help(add_value2)


if __name__ == '__main__':
	main()
