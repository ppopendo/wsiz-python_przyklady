# -*- coding: utf-8 -*-
"""Przykład dla definiowania klasy"""


class Product:
	"""
	Definicja produktu do sprzedaży
	"""

	def __init__(self, product_name: str, product_price: float, product_count: int):
		"""
		Parametry początkowe produktu
		:param product_name: nazwa produktu
		:param product_price: cena produktu
		:param product_count: ilość
		"""
		self.__product_name = product_name
		self.__product_price = product_price
		self.__product_count = product_count
		self.__is_sale = False

	@property
	def is_sale(self):
		"""
		Pobranie wartości dla is_sale
		:return: znacznik czy produkt jest sprzedawany
		"""
		return self.__is_sale

	@is_sale.setter
	def is_sale(self, is_sale: bool):
		"""
		Zmiana wartości is_sale
		:param is_sale: znacznik sprzedaży
		:return: zmiana wartości dla znacznika sprzedaży
		"""
		self.__is_sale = is_sale

	def show_product(self):
		"""
		Wyświetlenie produktu
		:return: produkt
		"""
		product_str = f'Nazwa produktu:{self.__product_name}\nCena produktu:{self.__product_price}\n' \
			f'Ilość:{self.__product_count}'
		print('-' * 100)
		print(product_str)
		print('-' * 100)


def main():
	"""
	Funkcja główna
	:return: None
	"""
	product_name = input('Podaj nazwę produktu:\n')
	product_price = round(float(input('Podaj cenę produktu:\n')))
	product_count = int(input('Podaj ilość:\n'))
	product = Product(
		product_name=product_name,  product_price=product_price, product_count=product_count
	)
	if product.is_sale:
		print('Produkt jest w sprzedaży')
	else:
		product.is_sale = True
		print('Product dodany do sprzedaży')
	product.show_product()


if __name__ == '__main__':
	main()
